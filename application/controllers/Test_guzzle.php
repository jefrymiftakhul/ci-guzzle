<?php
//require 'vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');
use GuzzleHttp\client;

class Test_guzzle extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('guzzle');
	}

	public function coba(){


		  # guzzle client define
		  $client     = new GuzzleHttp\Client();
		  
		  #This url define speific Target for guzzle
		  $url = "http://localhost/test/api/insert";

		  #guzzle
		  
		  try {
		    # guzzle post request example with form parameter
		    $response = $client->request( 'POST', 
		                                   $url, 
		                                  [ 'form_params' 
		                                        => [ 'first_name' => 'jjj','last_name' => 'aaaa' ] 
		                                  ]
		                                );
		    #guzzle repose for future use
		    echo $response->getStatusCode(); // 200
		    echo $response->getReasonPhrase(); // OK
		    echo $response->getProtocolVersion(); // 1.1
		    echo $response->getBody();
		  } catch (GuzzleHttp\Exception\BadResponseException $e) {
				    #guzzle repose for future use
				    $response = $e->getResponse();
				    $responseBodyAsString = $response->getBody()->getContents();
				    print_r($responseBodyAsString);
		  }
    }

}